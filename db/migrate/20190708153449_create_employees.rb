class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.date :admitted_at
      t.string :occupation

      t.timestamps
    end
  end
end
