class CreateProcessos < ActiveRecord::Migration[5.2]
  def change
    create_table :processos do |t|
      t.string :process
      t.string :kind
      t.date :draft_date
      t.string :draft_author
      t.date :check_date

      t.timestamps
    end
  end
end
