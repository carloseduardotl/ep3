# Fórum

### Geral:

Essa aplicação tem a capacidade de gerenciar processos sentênciados de um fórum e cadastrar a lista de funcionários

### Usuários:

Existem 3 tipos de usuários:

* Admin
* Editor
* Normal

### Acesso:

##### Admin:
Ele pode acessar os processos e apaga-los, porém não pode adicionar novos nem editar os existentes. Também pode adicionar novos funcionários

##### Editor:
Ele pode acessar os processos, adicionar novos e edita-los, mas somente o Admin pode apagar. Pode vizualizar os funcionários somente

##### Normal:
Não tem acesso aos processos, somente pode vizualizar os funcionários


## Instruções:
É preciso assegurar que todas as gems estão instaladas corretamentes, para isso execute:
> bundle install

Para criar as tabelas e migrar para o banco de dados execute:
> rake db:create db:migrate

Após isso inicie a aplicação com
> rails server

Por padrão o ambiente de desenvolvimento será executado em [localhost:3000](localhost:3000).


Novos usuários são criados como Normal, para alterar o tipo é necessário mudar diretamente no banco de dados. É possível fazer isso através do rails console. O seguinte exemplo muda o 1º usuário para admin:
> rails console
```
x = User.first
x.role = :admin
x.save
exit
```


## Observações:
O banco de dados utilizado foi sqlite3.

A aplicação foi desenvolvida com as seguintes versões:
* Ruby: 2.6.3
* Rails: 5.2.3
* Sqlite3: 3.28.0


As demais gems estão incluidas na gemfile.

A aplicação foi desenvolvida no Manjaro 18.0
