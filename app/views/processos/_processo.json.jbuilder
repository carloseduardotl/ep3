json.extract! processo, :id, :process, :kind, :draft_date, :draft_author, :check_date, :created_at, :updated_at
json.url processo_url(processo, format: :json)
