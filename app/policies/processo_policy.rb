class ProcessoPolicy < ApplicationPolicy

	def index?
		user.admin? || user.editor?		
	end

	def show?
		user.admin? || user.editor?	
	end

	def new?
		user.editor?	
	end

	def edit?
		user.editor?	
	end

	def destroy?
		user.admin?	
	end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
