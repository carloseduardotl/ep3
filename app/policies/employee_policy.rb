class EmployeePolicy < ApplicationPolicy

	def show?
		user.admin? || user.editor?	
	end

	def new?
		user.admin?	
	end

	def edit?
		user.admin?	
	end

	def destroy?
		user.admin?	
	end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
