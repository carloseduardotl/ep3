class ControlUsersController < ApplicationController
	before_action :authenticate_user!

  def index
  	@control_users = User.all
  	authorize @control_users
  end
end
