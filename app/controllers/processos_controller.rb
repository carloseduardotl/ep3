class ProcessosController < ApplicationController
  before_action :set_processo, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  # GET /processos
  # GET /processos.json
  def index
    @q = Processo.ransack(params[:q])
    @processos = @q.result

    @processos = @processos.page(params[:page]).per(10)
    authorize @processos
  end

  # GET /processos/1
  # GET /processos/1.json
  def show
    authorize @processo
  end

  # GET /processos/new
  def new
    @processo = Processo.new
    authorize @processo
  end

  # GET /processos/1/edit
  def edit
    authorize @processo
  end

  # POST /processos
  # POST /processos.json
  def create
    @processo = Processo.new(processo_params)

    respond_to do |format|
      if @processo.save
        format.html { redirect_to @processo, notice: 'Processo foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @processo }
      else
        format.html { render :new }
        format.json { render json: @processo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /processos/1
  # PATCH/PUT /processos/1.json
  def update
    respond_to do |format|
      if @processo.update(processo_params)
        format.html { redirect_to @processo, notice: 'Processo foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @processo }
      else
        format.html { render :edit }
        format.json { render json: @processo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /processos/1
  # DELETE /processos/1.json
  def destroy
    authorize @processo
    @processo.destroy
    respond_to do |format|
      format.html { redirect_to processos_url, notice: 'Processo foi apagado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_processo
      @processo = Processo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def processo_params
      params.require(:processo).permit(:process, :kind, :draft_date, :draft_author, :check_date)
    end
end
