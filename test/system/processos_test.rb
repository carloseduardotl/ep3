require "application_system_test_case"

class ProcessosTest < ApplicationSystemTestCase
  setup do
    @processo = processos(:one)
  end

  test "visiting the index" do
    visit processos_url
    assert_selector "h1", text: "Processos"
  end

  test "creating a Processo" do
    visit processos_url
    click_on "New Processo"

    fill_in "Check date", with: @processo.check_date
    fill_in "Draft author", with: @processo.draft_author
    fill_in "Draft date", with: @processo.draft_date
    fill_in "Kind", with: @processo.kind
    fill_in "Process", with: @processo.process
    click_on "Create Processo"

    assert_text "Processo was successfully created"
    click_on "Back"
  end

  test "updating a Processo" do
    visit processos_url
    click_on "Edit", match: :first

    fill_in "Check date", with: @processo.check_date
    fill_in "Draft author", with: @processo.draft_author
    fill_in "Draft date", with: @processo.draft_date
    fill_in "Kind", with: @processo.kind
    fill_in "Process", with: @processo.process
    click_on "Update Processo"

    assert_text "Processo was successfully updated"
    click_on "Back"
  end

  test "destroying a Processo" do
    visit processos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Processo was successfully destroyed"
  end
end
